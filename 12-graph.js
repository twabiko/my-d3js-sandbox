var svg = d3.select("svg");
var width = parseInt(svg.attr("width"));
var height = parseInt(svg.attr("height"));

// https://github.com/d3/d3-scale/blob/master/README.md#scaleOrdinal
var color = d3.scaleOrdinal(d3.schemeCategory20);

// https://github.com/d3/d3/blob/master/API.md#forces-d3-force
var simulation = d3.forceSimulation()
		.force("link", d3.forceLink().id(function(d) { return d.id; })) // json の ID を返す
		.force("charge", d3.forceManyBody())                            // ノード間を拡げる
		.force("center", d3.forceCenter(width / 2, height / 2));        // 描画を真ん中に

d3.json("miserables.json", function(error, graph) {
	// 読み込んだ json が graph で参照できる

	if (error) throw error;

	// <g> はオブジェクトをグループ化するもの
	var link = svg.append("g")
			.attr("class", "links")
		.selectAll("line")
		.data(graph.links)
		.enter().append("line") // オブジェクトがなければ作る
			.attr("stroke-width", function(d) { return Math.sqrt(d.value); }); // 線の太さをいい感じにする

	// ノードを作る
	var node = svg.append("g")
			.attr("class", "nodes")
		.selectAll("circle")
		.data(graph.nodes)
		.enter().append("circle")
			.attr("r", 5)
			.attr("fill", function(d) { return color(d.group); })
			.call(d3.drag()
					.on("start", dragstarted)
					.on("drag", dragged)
					.on("end", dragended));

	// ノードのテキストを id にする
	node.append("title")
			.text(function(d) { return d.id; });

	simulation
			.nodes(graph.nodes)
			.on("tick", ticked)
	;

	simulation.force("link")
			.links(graph.links);

	function ticked() {
		// 線座標 (x1,y1)-(x2,y2) を返す
		link
				.attr("x1", function(d) { return d.source.x; })
				.attr("y1", function(d) { return d.source.y; })
				.attr("x2", function(d) { return d.target.x; })
				.attr("y2", function(d) { return d.target.y; });

		// ノードの位置を返す
		node
				.attr("cx", function(d) { return d.x; })
				.attr("cy", function(d) { return d.y; });
	}
});

// ノード情報の中に座標情報が入って d に格納されてる
function dragstarted(d) {
	if (!d3.event.active) simulation.alphaTarget(0.3).restart();
	d.fx = d.x;
	d.fy = d.y;
}

function dragged(d) {
	d.fx = d3.event.x;
	d.fy = d3.event.y;
}

function dragended(d) {
	if (!d3.event.active) simulation.alphaTarget(0);
	d.fx = null;
	d.fy = null;
}

console.log('xxx');
